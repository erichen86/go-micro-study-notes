# go-micro-study-notes

我在csdn创作一系列GO语言微服务框架`go-micro`的学习笔记，主要记录我在go-micro学习摸索中遇到的问题，这里则用于存放笔记相关代码

专栏地址：[csdn](https://blog.csdn.net/hotcoffie/category_10375782.html) / [知乎](https://zhuanlan.zhihu.com/c_1290248879594655744)

# 第一部分 从无到有
参考官方demo编写简单服务，实现服务注册、发现、消息订阅和API网关:

（一）使用micro工具自动生成项目
[笔记](https://blog.csdn.net/hotcoffie/article/details/108464507)

（二）手写第一个微服务
[笔记](https://blog.csdn.net/hotcoffie/article/details/108512271)

（三）消息的订阅和发布
[笔记](https://blog.csdn.net/hotcoffie/article/details/108643839)

（四）集成micro api网关
[笔记](https://blog.csdn.net/hotcoffie/article/details/108527948)

# 第二部分 集成第三方工具
演示各种第三方工具如何快速集成到服务中

（五）集成etcd注册中心
[笔记](https://blog.csdn.net/hotcoffie/article/details/108750996)

（六）集成nats消息中间件
[笔记](https://blog.csdn.net/hotcoffie/article/details/108791412)

（七）集成断路器Hystrix
[笔记](https://blog.csdn.net/hotcoffie/article/details/108844945)

（八）集成链路追踪工具jaeger
[笔记](https://blog.csdn.net/hotcoffie/article/details/108814778)

（九）定制网关（1）——集成JWT鉴权
[笔记](https://blog.csdn.net/hotcoffie/article/details/109062463)

（十）定制网关（2）——集成断路器Hystrix
[笔记](https://blog.csdn.net/hotcoffie/article/details/109183219)

# 支持一下
原创不易，买杯咖啡，谢谢:p

![支持一下](https://img-blog.csdnimg.cn/20200910130505778.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2hvdGNvZmZpZQ==,size_16,color_FFFFFF,t_70#pic_center)

