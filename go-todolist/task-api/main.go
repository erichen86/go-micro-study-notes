package main

import (
	hystrixGo "github.com/afex/hystrix-go/hystrix"
	"github.com/gin-gonic/gin"
	"github.com/micro/go-micro/v2"
	"github.com/micro/go-micro/v2/registry"
	"github.com/micro/go-micro/v2/registry/etcd"
	"github.com/micro/go-micro/v2/web"
	"github.com/micro/go-plugins/wrapper/trace/opentracing/v2"
	"go-todolist/common/tracer"
	"go-todolist/task-api/handler"

	// 官方插件
	// "github.com/micro/go-plugins/wrapper/breaker/hystrix/v2"
	// 自定义插件
	"go-todolist/task-api/wrapper/breaker/hystrix"
	pb "go-todolist/task-srv/proto/task"
	"log"
)

const (
	ServerName = "go.micro.api.task"
	EtcdAddr   = "172.18.0.58:2379"
	JaegerAddr = "172.18.0.58:6831"
)

// task-srv服务的restful api映射
func main() {
	// 配置jaeger连接
	jaegerTracer, closer, err := tracer.NewJaegerTracer(ServerName, JaegerAddr)
	if err != nil {
		log.Fatal(err)
	}
	defer closer.Close()

	etcdRegister := etcd.NewRegistry(
		registry.Addrs(EtcdAddr),
	)
	// 之前我们使用client.DefaultClient注入到pb.NewTaskService中
	// 现在改为标准的服务创建方式创建服务对象
	// 但这个服务并不真的运行（我们并不调用他的Init()和Run()方法）
	// 如果是task-srv这类本来就是用micro.NewService服务创建的服务
	// 则直接增加包装器，不需要再额外新增服务
	app := micro.NewService(
		micro.Name("go.micro.client.task"),
		micro.Registry(etcdRegister),
		micro.WrapClient(
			// 引入hystrix包装器
			hystrix.NewClientWrapper(),
			// 配置链路追踪为jaeger
			opentracing.NewClientWrapper(jaegerTracer),
		),
	)
	// 自定义全局默认超时时间和最大并发数
	hystrixGo.DefaultSleepWindow = 300
	hystrixGo.DefaultMaxConcurrent = 3

	// 针对指定服务接口使用不同熔断配置
	// 第一个参数name=服务名.接口.方法名，这并不是固定写法，而是因为官方plugin默认用这种方式拼接命令name
	// 之后我们自定义wrapper也同样使用了这种格式
	// 如果你采用了不同的name定义方式则以你的自定义格式为准
	hystrixGo.ConfigureCommand("go.micro.service.task.TaskService.Search",
		hystrixGo.CommandConfig{
			MaxConcurrentRequests: 50,
			Timeout:               300,
		})
	taskService := pb.NewTaskService("go.micro.service.task", app.Client())

	webHandler := gin.Default()
	// 这个服务才是真正运行的服务
	service := web.NewService(
		web.Name("go.micro.api.task"),
		web.Address(":8888"),
		web.Handler(webHandler),
		web.Registry(etcdRegister),
	)

	// 配置web路由
	handler.Router(webHandler, taskService)

	service.Init()
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
