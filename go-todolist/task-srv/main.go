package main

import (
	"context"
	"fmt"
	"github.com/micro/go-micro/v2"
	"github.com/micro/go-micro/v2/broker"
	"github.com/micro/go-micro/v2/broker/nats"
	"github.com/micro/go-micro/v2/registry"
	"github.com/micro/go-micro/v2/registry/etcd"
	"github.com/micro/go-plugins/wrapper/trace/opentracing/v2"
	"github.com/pkg/errors"
	"go-todolist/common/db"
	"go-todolist/common/tracer"
	"go-todolist/task-srv/handler"
	pb "go-todolist/task-srv/proto/task"
	"go-todolist/task-srv/repository"
	"log"
	"time"
)

// 所有port都是服务默认端口，ip请根据实际情况配置
const (
	ServerName = "go.micro.service.task"
	MongoUri   = "mongodb://172.18.0.58:27017"
	NatsUri    = "nats://172.18.0.58:4222"
	EtcdAddr   = "172.18.0.58:2379"
	JaegerAddr = "172.18.0.58:6831"
)

// task-srv服务
func main() {

	conn, err := db.ConnectMongo(MongoUri, time.Second)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Disconnect(context.Background())

	// 配置jaeger连接
	jaegerTracer, closer, err := tracer.NewJaegerTracer(ServerName, JaegerAddr)
	if err != nil {
		log.Fatal(err)
	}
	defer closer.Close()

	// New Service
	service := micro.NewService(
		micro.Name(ServerName),
		micro.Version("latest"),
		// 配置etcd为注册中心，配置etcd路径，默认端口是2379
		micro.Registry(etcd.NewRegistry(
			// 地址是我本地etcd服务器地址，不要照抄
			registry.Addrs(EtcdAddr),
		)),
		// 配置nats为消息中间件，默认端口是4222
		micro.Broker(nats.NewBroker(
			// 地址是我本地nats服务器地址，不要照抄
			broker.Addrs(NatsUri),
		)),
		// 配置链路追踪为jaeger
		micro.WrapHandler(opentracing.NewHandlerWrapper(jaegerTracer)),
	)

	// Initialise service
	service.Init()

	// Register Handler
	taskHandler := &handler.TaskHandler{
		TaskRepository: &repository.TaskRepositoryImpl{
			Conn: conn,
		},
		// 注入消息发送实例,为避免消息名冲突,这里的topic我们用服务名+自定义消息名拼出
		TaskFinishedPubEvent: micro.NewEvent(fmt.Sprintf("%s.%s", ServerName, handler.TaskFinishedTopic), service.Client()),
	}
	if err := pb.RegisterTaskServiceHandler(service.Server(), taskHandler); err != nil {
		log.Fatal(errors.WithMessage(err, "register server"))
	}

	// Run service
	if err := service.Run(); err != nil {
		log.Fatal(errors.WithMessage(err, "run server"))
	}
}
